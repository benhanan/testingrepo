const api_url =
    "https://jsonplaceholder.typicode.com/users";

// Defining async function
async function getapi(url) {

    // Storing response
    let request = await new XMLHttpRequest();
    request.open("GET", url);
    request.send();
    request.onload = () => {
        // console.log(request);
        if (request.status === 200) {
            // by default the response comes in the string format, we need to parse the data into JSON
            // Storing data in form of JSON
            var data = JSON.parse(request.response);
            console.log(data);
            if (request) {
                hideloader();
            }
            show(data);
        } else {
            console.log(`error ${request.status} ${request.statusText}`);
        }
    };
}
// Calling that async function
getapi(api_url);

// Function to hide the loader
function hideloader() {
    document.getElementById('loading').style.display = 'none';
}
// Function to define innerHTML for HTML table
function show(data) {
    let tab =
        `<tr>
          <th>Name</th>
          <th>Office</th>
          <th>Position</th>
          <th>Salary</th>
         </tr>`;

    // Loop to access all rows 
    for (let r of data) {
        tab += `<tr> 
    <td>${r.name} </td>
    <td>${r.username}</td>
    <td>${r.email}</td> 
    <td>${r.website}</td>          
</tr>`;
    }
    // Setting innerHTML as tab variable
    document.getElementById("employees").innerHTML = tab;
}